class WeatherWidget {
    constructor(api){
        this.api = api;
    }
    getWeather(coords){
        let param = 'lat=' + parseFloat((coords.latitude).toFixed(6)) + '&lon=' + parseFloat((coords.longitude).toFixed(6));
        this.api = 'http://api.openweathermap.org/data/2.5/forecast?lang=ru&' + param + '&appid=81744238b310fc03e93b3ccf06c28ec1'
        fetch(this.api)  
        .then(function(resp) {return resp.json()}) 
        .then(function(d) {
            const {list} = d;
            let days = [];
            list.slice(0, 24).map(item => {
                if(item.dt_txt.includes('12:00:00')){
                    days.push(item);
                    }
                })

            let icon1 = document.querySelector(".img1");
            icon1.src = `http://openweathermap.org/img/w/${days[0].weather[0].icon}.png`;  
            let date1 = days[0].dt_txt;
            date1 = date1.split(" ")[0];
            let descr1 = days[0].weather[0].description;
            document.querySelector(".description1").innerHTML = descr1;
            let temp1 = Math.round(parseFloat(days[0].main.temp)-273.15);
            document.getElementById("temp1").innerHTML = `${date1} в<br /> ${d.city.name}<br /> днем ${temp1}&deg;`;
            let feels1 = Math.round(parseFloat(days[0].main.feels_like)-273.15);
            document.querySelector('#feels1').innerHTML = `По ощущению ${feels1}&deg;`;
            let wind1 = Math.round(parseFloat(days[0].wind.speed));
            document.querySelector('#wind1').innerHTML = `Скорость ветра ${wind1} м/с`;

            let icon2 = document.querySelector(".img2");
            icon2.src = `http://openweathermap.org/img/w/${days[1].weather[0].icon}.png`;
            let date2 = days[1].dt_txt;
            date2 = date2.split(" ")[0];    
            let descr2 = days[1].weather[0].description;
            document.querySelector(".description2").innerHTML = descr2;
            let temp2 = Math.round(parseFloat(days[1].main.temp)-273.15);
            document.getElementById("temp2").innerHTML = `${date2} в<br /> ${d.city.name}<br /> днем ${temp2}&deg;`;
            let feels2 = Math.round(parseFloat(days[1].main.feels_like)-273.15);
            document.querySelector('#feels2').innerHTML = `По ощущению ${feels2}&deg;`;
            let wind2 = Math.round(parseFloat(days[1].wind.speed));
            document.querySelector('#wind2').innerHTML = `Скорость ветра ${wind2} м/с`;

            let icon3 = document.querySelector(".img3");
            icon3.src = `http://openweathermap.org/img/w/${days[2].weather[0].icon}.png`;   
            let date3 = days[2].dt_txt;
            date3 = date3.split(" ")[0];  
            let descr3 = days[2].weather[0].description;
            document.querySelector(".description3").innerHTML = descr3;
            let temp3 = Math.round(parseFloat(days[2].main.temp)-273.15);
            document.getElementById("temp3").innerHTML = `${date3} в<br /> ${d.city.name}<br /> днем ${temp3}&deg;`;
            let feels3 = Math.round(parseFloat(days[2].main.feels_like)-273.15);
            document.querySelector('#feels3').innerHTML = `По ощущению ${feels3}&deg;`;
            let wind3 = Math.round(parseFloat(days[2].wind.speed));
            document.querySelector('#wind3').innerHTML = `Скорость ветра ${wind3} м/с`;
            
            

            let celcius = Math.round(parseFloat(d.list[0].main.temp)-273.15);
            let feels = Math.round(parseFloat(d.list[0].main.feels_like)-273.15);
            let description = d.list[0].weather[0].description;
            let wind = Math.round(parseFloat(d.list[0].wind.speed));
            let icon = document.querySelector('.img');
            icon.src = `http://openweathermap.org/img/w/${d.list[0].weather[0].icon}.png`;
            if(d.city.name == 'Минск'){
                d.city.name = 'Минске'
            }
            
            document.querySelector('.description').innerHTML = description;
            document.querySelector('.temp').innerHTML = `Сейчас в<br /> ${d.city.name} ${celcius}&deg;`;
            document.querySelector('.feels').innerHTML = `По ощущению ${feels}&deg;`;
            document.querySelector('.wind').innerHTML = `Скорость ветра ${wind} м/с`;
            
            if( description == ('небольшой дождь' || 'дождь')) {
            document.body.className = 'rainy';
            } else if( description == ('облачно' || 'небольшая облачность') > 0 ) {
                document.body.className = 'cloudy';
            } else if( description == ('солнечно' || 'ясно') > 0 ) {
                document.body.className = 'sunny';
            }
    })
    .catch(function() {
        console.warn('err');
    });
    }

    getWeather5(){
        this.api = 'http://api.openweathermap.org/data/2.5/forecast?lang=ru&q=minsk&appid=81744238b310fc03e93b3ccf06c28ec1'
        fetch(this.api)  
        .then(function(resp) { 
            return resp.json() }) 
        .then(function(d) {
            const {list} = d;
            let days = [];
            list.slice(0, 24).map(item => {
                if(item.dt_txt.includes('12:00:00')){
                    days.push(item);
                    }
                })

            let icon1 = document.querySelector(".img1");
            icon1.src = `http://openweathermap.org/img/w/${days[0].weather[0].icon}.png`;  
            let date1 = days[0].dt_txt;
            date1 = date1.split(" ")[0];
            let descr1 = days[0].weather[0].description;
            document.querySelector(".description1").innerHTML = descr1;
            let temp1 = Math.round(parseFloat(days[0].main.temp)-273.15);
            document.getElementById("temp1").innerHTML = `${date1} в<br /> ${d.city.name}<br /> днем ${temp1}&deg;`;
            let feels1 = Math.round(parseFloat(days[0].main.feels_like)-273.15);
            document.querySelector('#feels1').innerHTML = `По ощущению ${feels1}&deg;`;
            let wind1 = Math.round(parseFloat(days[0].wind.speed));
            document.querySelector('#wind1').innerHTML = `Скорость ветра ${wind1} м/с`;

            let icon2 = document.querySelector(".img2");
            icon2.src = `http://openweathermap.org/img/w/${days[1].weather[0].icon}.png`;
            let date2 = days[1].dt_txt;
            date2 = date2.split(" ")[0];    
            let descr2 = days[1].weather[0].description;
            document.querySelector(".description2").innerHTML = descr2;
            let temp2 = Math.round(parseFloat(days[1].main.temp)-273.15);
            document.getElementById("temp2").innerHTML = `${date2} в<br /> ${d.city.name}<br /> днем ${temp2}&deg;`;
            let feels2 = Math.round(parseFloat(days[1].main.feels_like)-273.15);
            document.querySelector('#feels2').innerHTML = `По ощущению ${feels2}&deg;`;
            let wind2 = Math.round(parseFloat(days[1].wind.speed));
            document.querySelector('#wind2').innerHTML = `Скорость ветра ${wind2} м/с`;

            let icon3 = document.querySelector(".img3");
            icon3.src = `http://openweathermap.org/img/w/${days[2].weather[0].icon}.png`;   
            let date3 = days[2].dt_txt;
            date3 = date3.split(" ")[0];  
            let descr3 = days[2].weather[0].description;
            document.querySelector(".description3").innerHTML = descr3;
            let temp3 = Math.round(parseFloat(days[2].main.temp)-273.15);
            document.getElementById("temp3").innerHTML = `${date3} в<br /> ${d.city.name}<br /> днем ${temp3}&deg;`;
            let feels3 = Math.round(parseFloat(days[2].main.feels_like)-273.15);
            document.querySelector('#feels3').innerHTML = `По ощущению ${feels3}&deg;`;
            let wind3 = Math.round(parseFloat(days[2].wind.speed));
            document.querySelector('#wind3').innerHTML = `Скорость ветра ${wind3} м/с`;

            let celcius = Math.round(parseFloat(d.list[0].main.temp)-273.15);
            let feels = Math.round(parseFloat(d.list[0].main.feels_like)-273.15);
            let description = d.list[0].weather[0].description;
            let wind = Math.round(parseFloat(d.list[0].wind.speed));
            let icon = document.querySelector('img');
            icon.src = `http://openweathermap.org/img/w/${d.list[0].weather[0].icon}.png`;
            if(d.city.name == 'Минск'){
                d.city.name = 'Минске'
            }
            
            document.querySelector('.description').innerHTML = description;
            document.querySelector('.temp').innerHTML = `Сейчас в<br /> ${d.city.name} ${celcius}&deg;`;
            document.querySelector('.feels').innerHTML = `По ощущению ${feels}&deg;`;
            document.querySelector('.wind').innerHTML = `Скорость ветра ${wind} м/с`;
            
            if( description == ('небольшой дождь' || 'дождь')) {
            document.body.className = 'rainy';
            } else if( description == ('облачно' || 'небольшая облачность') > 0 ) {
                document.body.className = 'cloudy';
            } else if( description == ('солнечно' || 'ясно') > 0 ) {
                document.body.className = 'sunny';
            }
            
    })
    .catch(function() {
        console.warn("err");
    });
    }
}
const catchWeather = new WeatherWidget();
catchWeather.getWeather5();

window.onload = function() {
    navigator.geolocation.getCurrentPosition(({coords}) => {
        catchWeather.getWeather(coords);
    })
  }
